# tkai-final-project

A project for academic purpose

## Folder Structure Overview

```
frontend
│   README.md
│       
└───folder1
│   │   file011.txt
│   │   ...
│   │
│   └───subfolder1
│       │   file111.txt
│       │   ...
│   
└───folder2
    │   file021.txt
 
    
    
backend-service-1
│   README.md
│       
└───folder1
│   │   file011.txt
│   │   ...
│   │
│   └───subfolder1
│       │   file111.txt
│       │   ...
│   
└───folder2
    │   file021.txt
 
    
backend-service-2
│   README.md
│       
└───folder1
│   │   file011.txt
│   │   ...
│   │
│   └───subfolder1
│       │   file111.txt
│       │   ...
│   
└───folder2
    │   file021.txt
 
```

